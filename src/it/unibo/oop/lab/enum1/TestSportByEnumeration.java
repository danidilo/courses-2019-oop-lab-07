package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        
    	SportSocialNetworkUserImpl<User> usr1 = new SportSocialNetworkUserImpl<User>("Daniele", "Di Lillo", "djDoppiaD");
    	SportSocialNetworkUserImpl<User> usr2 = new SportSocialNetworkUserImpl<User>("Riccardo", "Albertini", "autisticKid");
    	SportSocialNetworkUserImpl<User> usr3 = new SportSocialNetworkUserImpl<User>("Andrea", "Brighi", "Il Terminatore");
    
    	usr1.addSport(Sport.SOCCER);
    	usr1.addSport(Sport.SKATE);
    	usr1.addSport(Sport.F1);
    	usr1.addSport(Sport.MOTOGP);
    	
    	System.out.println("Daniele loves skateboarding? " + usr1.hasSport(Sport.SKATE) + 
    					   " and likes playing tennis? " + !usr1.hasSport(Sport.TENNIS));
    	
    	usr2.addSport(Sport.BASKET);
    	usr2.addSport(Sport.SOCCER);
    	usr2.addSport(Sport.BIKE);
    	usr2.addSport(Sport.VOLLEY);
    	
    	usr3.addSport(Sport.SOCCER);
    	usr3.addSport(Sport.BASKET);
    	
    	
    	
    }

}
